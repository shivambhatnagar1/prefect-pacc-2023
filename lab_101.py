import requests
from prefect import flow, task


# Lab 101 - Weather

@task
def get_weather(lat: float, lon: float):
    base_url = "https://api.open-meteo.com/v1/forecast"
    weather = requests.get(
        url=base_url,
        params={
            "latitude": lat,
            "longitude": lon,
            "daily": "temperature_2m_max",
            "timezone": "GMT"
        }
    )
    daily_weather_data = weather.json()
    return daily_weather_data

@task
def format_weather_data(raw_data):
    daily_weather_data = raw_data["daily"]["temperature_2m_max"]
    return daily_weather_data


@task
def save_weather(data: list):
    with open("weather.csv", "w+") as w:
        for item in data:
            w.write(item)
    return "Successfully wrote daily temperature"


@flow()
def workflow(lat: float, lon: float):
    weather_data = get_weather(lat, lon)
    daily_weather_data = format_weather_data(weather_data)
    result = save_weather(str(daily_weather_data))
    return result


if __name__ == '__main__':
    workflow(38.9, -77.0)