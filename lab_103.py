import os

import requests
from prefect import flow, task
from prefect.tasks import task_input_hash
from datetime import timedelta
from prefect_email import EmailServerCredentials, email_send_message


# Prefect Base Args
WORKFLOW_NAME = 'LAB103_RETRY_CACHE'


# Lab 103 - Weather
# Create Email Block

@task(
    retries=2,
    cache_key_fn=task_input_hash,
    cache_expiration = timedelta(minutes=1)
)
def create_email_block(email_address: str):
    email_server_credentials = EmailServerCredentials(
        username=email_address,
        password=os.environ["EMAIL_PASSWORD"],
    )
    email_server_credentials.save("person-shiv-email", overwrite=True)
    return "Successfully created email block"


@task(
    retries=2,
    retry_delay_seconds=0.1,
    cache_key_fn=task_input_hash,
    cache_expiration=timedelta(minutes=1)
    )
def get_weather(lat: float, lon: float):
    base_url = "https://api.open-meteo.com/v1/forecast"
    weather = requests.get(
        url=base_url,
        params={
            "latitude": lat,
            "longitude": lon,
            "daily": "temperature_2m_max",
            "timezone": "GMT"
        }
    )
    daily_weather_data = weather.json()
    return daily_weather_data


@task(
    cache_key_fn=task_input_hash,
    cache_expiration=timedelta(minutes=1)
)
def format_weather_data(raw_data):
    daily_weather_data = raw_data["daily"]["temperature_2m_max"]
    return daily_weather_data


@task(retries=2, retry_delay_seconds=0.1)
def save_weather(data: list):
    with open("weather.csv", "w+") as w:
        for item in data:
            w.write(item)
    return "Successfully wrote daily temperature"


@flow(name=WORKFLOW_NAME, retries=1)
def workflow(lat: float, lon: float):
    create_email_block('shivam1997bhatnagar@gmail.com')
    weather_data = get_weather(lat, lon)
    daily_weather_data = format_weather_data(weather_data)
    result = save_weather(str(daily_weather_data))
    email_credentials = EmailServerCredentials.load("person-shiv-email")
    email_send_message.with_options(
        name=f"email {'shivam1997bhatnagar@gmail.com'}").submit(
        email_server_credentials=email_credentials,
        subject="Successful Flow",
        msg="Test Success",
        email_to="shivam1997bhatnagar@gmail.com"
    )
    return result


if __name__ == '__main__':
    workflow(38.9, -77.0)